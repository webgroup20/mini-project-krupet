// Function in JS
// declare function
function greeting(){
    // body function contain statement
    console.log("Greeting function executed!");
}

// () => body function
let greetingA = () => console.log("Greeting arrow function executed!");
greetingA()

let resultArrow = () => {
    console.log("hello");
    console.log("helo");
}

// execute
// greeting()
//---- function with parameter
let h1 = document.querySelector("h1");
function greetingMessage(name = 'Bopha'){
    h1.innerHTML = `Welcome, ${name}`;
}
// greetingMessage("Chhaya")

// -- function with return 
function sum(num1 = 10, num2 = 10){
    let result = num1 + num2;
    return result;
}
let sumResult = sum(30);
console.log(sumResult);

function divide(num1 = 10, num2){
    let result = num1 + num2;
    return result;
}
let divideResult = divide(30, "3");
console.log(divideResult);
// console.clear()
function test(num1, num2){
    let result = num1 + num2;
    return result;
}

// let num1 = prompt("Enter num1");
// let num2 = prompt("Enter num2");

// let testResult = test(parseFloat(num1), parseFloat(num2));
// console.log(testResult);

/// Array with Arrow function
// let students = [
//     {
//         name: "Chhaya",
//         profile: "https://eduport.webestica.com/assets/images/avatar/01.jpg"
//     },
//     {
//         name: "Pov",
//         profile: "https://eduport.webestica.com/assets/images/avatar/02.jpg"
//     },
//     {
//         name: "Soklin",
//         profile: "https://eduport.webestica.com/assets/images/avatar/03.jpg"
//     },
//     {
//         name: "Mouykea",
//         profile: "https://eduport.webestica.com/assets/images/avatar/04.jpg"
//     },
//     {
//         name: "Oudom",
//         profile: "https://eduport.webestica.com/assets/images/avatar/05.jpg"
//     },
//     {
//         name: "Senglong",
//         profile: "https://eduport.webestica.com/assets/images/avatar/06.jpg"
//     },
//     {
//         name: "Thach",
//         profile: "https://eduport.webestica.com/assets/images/avatar/06.jpg"
//     },
//     {
//         name: "Bunheng",
//         profile: "https://eduport.webestica.com/assets/images/avatar/06.jpg"
//     },
//     {
//         name: "Somnang",
//         profile: "https://eduport.webestica.com/assets/images/avatar/06.jpg"
//     }
// ];

// students.map((student, index) => {
//     console.log(student, "index: ", index);
// })

let BASE_URL = 'https://api.escuelajs.co/api/v1/';
function fetchProducts(){
    fetch(`https://api.escuelajs.co/api/v1/products`)
    .then((response) => response.json())
    .then((data) => {
        // data response from api
        data.slice(0, 10).map((product) => {
            h1.innerHTML += `<h3>${product.title}</h3>`
        })
    })
}
fetchProducts()


