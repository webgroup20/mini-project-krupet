// Operator in JS: special character for perform math
// 1. Assignment Operator ( =, +=, -=, *=, %=, /=)
let x = 90
let y = 100
console.log("x: ", x);
x += y // x = x + y
console.log("x: ", x);
y -= x; // y = y - x
console.log("y", y);

// 2. Arimethic Operator (+, -, *, /, %, ++, --, **)
let num1 = 5;
let num2 = 2;
let result = num1 + num2;
console.log("result: ", result);

let divideResult = num1 % num2;
console.log("divide result:", divideResult);
let increaseByOne = num1++;
console.log("increase by one:", increaseByOne);
console.log("num1: ", num1);
console.log("exponential: ", num2**4);

// 3. Comparision Operator (==, ===, >=, <=, !=, >, <)
let score = "10"
if (score == 10){
    console.log("You are top score", score);
}else{
    console.log("You are not top score", score);
}
//4. Logical Operator ( &&, ||, !)
// let gender = prompt("Enter gender");
// let age = prompt("Enter age");
// if (gender == "female" && age >= 18){
//     console.log("You are allowed to study here");
// }else{
//     console.log("You are not allowed to study here");
// }

// 5. String Operator
let message = "Hello";
message += ` World`;
console.log(message);

// 6. Ternary Operator
// syntax: condition ? "Expression_1" : "Expression_2"
// let age = prompt("Enter Your age");
// let msg = ( age >= 18 ) ? "You are allowed to vote" : "You are not";
// console.log(msg);

// 7. Nullish coaleascing assignment
let student = {
    age: 20
}
student.age ??= 30;
student.name ??= "Panha";
console.log(student);

// 8. Nullish Coalescing 
let title = undefined ?? "Web Design";
console.log(title);

/// JS only array for store collection data
let arr = ["Chhaya", "Pov", "Soklin", "Muykea", "Oudom", "Senglong"];
console.log(arr.length);
let students = [
    {
        name: "Chhaya",
        profile: "https://eduport.webestica.com/assets/images/avatar/01.jpg"
    },
    {
        name: "Pov",
        profile: "https://eduport.webestica.com/assets/images/avatar/02.jpg"
    },
    {
        name: "Soklin",
        profile: "https://eduport.webestica.com/assets/images/avatar/03.jpg"
    },
    {
        name: "Mouykea",
        profile: "https://eduport.webestica.com/assets/images/avatar/04.jpg"
    },
    {
        name: "Oudom",
        profile: "https://eduport.webestica.com/assets/images/avatar/05.jpg"
    },
    {
        name: "Senglong",
        profile: "https://eduport.webestica.com/assets/images/avatar/06.jpg"
    }
]
console.log(students);
console.log(students[0].name);

let card = "";
// loop follow item of array, will stop execute when condition false
for (i = 0; i<students.length; i++){
    console.log(students[i]);
    card += `
        <div class="max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
            <a href="#">
                <img class="rounded-t-lg" src=${students[i].profile} alt="" />
            </a>
            <div class="p-5">
                <a href="#">
                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">${students[i].name}</h5>
                </a>   
            </div>
        </div>
    `
}

// get html
let section = document.querySelector(".profile");
section.innerHTML = card;











