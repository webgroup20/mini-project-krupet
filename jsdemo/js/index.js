// ---Declare variable---
var firstName = "Dara"
let lastName = "Pov"
console.log(firstName, lastName);

var firstName = "Chhaiya"
console.log("second first name", firstName);
// variable is case sensitive
let LASTNAME = "Eric"
// valid naming rule
let numOfStudents;
numOfStudents = 10;
let _numOfTeachers = 3;
_numOfTeachers = 9;
console.log(_numOfTeachers);
let $numOfApples = 4;
// invalid naming rule
// let 5Number 

//---- Constant -----
const numOfPhone = 90; // can't change value
console.log(numOfPhone);



