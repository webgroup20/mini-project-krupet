// create array
let arr = new Array("Apple", "Google") // create empty array using constructor
let arr1 = ["Apple", "Google"] // literal notation
console.log(arr);
console.log(arr1);
console.log(arr1[arr1.length - 1]);

let students = ["Soklin", "Pov"];
students.unshift("Bunheng") // add new to start element
console.log(students);
students.push("Senglong") // add new to last element
console.log(students);
students.pop() // remove last item
console.log(students); // ['Bunheng', 'Soklin', 'Pov']
students.splice(0, 2, "Somnang", "Muykea", "Thach"); //remove from index 0 and 2 items was remove
console.log(students);
students.shift();
console.log(students);

console.clear()
let highSchoolStudent = ["Dara", "Panha"]
let totalStudents = students.concat(highSchoolStudent);
console.log(totalStudents);
console.log(totalStudents.slice(0, 3));

// map: array method
// console.log(totalStudents[0]);
// console.log(totalStudents[1]);
// console.log(totalStudents[2]);
// console.log(totalStudents[3]);
totalStudents.map(function(data, index){
    console.log("data:", data, "index:", index);
})

let todayClassStudents = [
    {
        name: "Chhaya",
        profile: "https://eduport.webestica.com/assets/images/avatar/01.jpg"
    },
    {
        name: "Pov",
        profile: "https://eduport.webestica.com/assets/images/avatar/02.jpg"
    },
    {
        name: "Soklin",
        profile: "https://eduport.webestica.com/assets/images/avatar/03.jpg"
    },
    {
        name: "Mouykea",
        profile: "https://eduport.webestica.com/assets/images/avatar/04.jpg"
    },
    {
        name: "Oudom",
        profile: "https://eduport.webestica.com/assets/images/avatar/05.jpg"
    },
    {
        name: "Senglong",
        profile: "https://eduport.webestica.com/assets/images/avatar/06.jpg"
    },
    {
        name: "Thach",
        profile: "https://eduport.webestica.com/assets/images/avatar/06.jpg"
    },
    {
        name: "Bunheng",
        profile: "https://eduport.webestica.com/assets/images/avatar/06.jpg"
    },
    {
        name: "Somnang",
        profile: "https://eduport.webestica.com/assets/images/avatar/06.jpg"
    }
];
console.clear()
console.log(todayClassStudents.length);

// manipulate with table
let tbody = document.querySelector("tbody");
todayClassStudents.slice(0, 3).map(function(student, index){
    tbody.innerHTML += `
            <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                    ${++index}
                </th>
                <td class="px-6 py-4">
                    ${student.name}
                </td>
                <td class="px-6 py-4">
                    <img class="w-10 h-10 rounded-full" src=${student.profile} alt="Rounded avatar">
                </td>
                <td class="px-6 py-4">
                    ${student.name}
                </td>
            </tr>
        `
})

// copy array
let copyArray = [...todayClassStudents];
console.log(copyArray);

