//------ DOM-----
// manipulate user interface with DOM method and properties
let H1 = document.getElementById("demo");
console.log(H1);
H1.innerHTML = "Good moring";
H1.setAttribute("class", "text-center text-darkblue");

// set event on button
let btnTest = document.querySelector("button");
let IMG = document.querySelector("img");
let title = document.querySelector("h5")
btnTest.addEventListener("click", function(){
    // body of function will execute when button clicked
    console.log("Button Press");
    IMG.src = "https://eduport.webestica.com/assets/images/courses/4by3/07.jpg"
    title.innerHTML = "Web Design"
})